---
layout: single
title: "OSU Open Source Lab"
permalink: /node/9
author: Corey Shields
---

A non-profit department of the University.

Primary goals of the OSU OSL is to help facilitate global FLOSS development.

The lab helps benefit the OSU by allowing it to offload some infrastructure and development needs to other projects.

Massive growth:

* 60 servers last year
* 165 servers this year
* new data center opening

Lots of safety on the infrastructure layer: Double-redundant UPS, generators can be topped off while running, etc.

OSL wants to provide appropriately prices services to the FLOSS community.

Incredible list of clients -> http://osuosl.org/hosting/clients

Not just a cookie-cutter shop

Donations have been provided by outside sources to cover costs. Mozilla and Google have been major sources of support.

## Expenses

* Electricity: About $30 / machine / month (or $500 / rack / month)
* Bandwidth: About $45 / meg / month
* Support staff
* Cheaper on power, more expensive on bandwidth

## Future Needs

* Determine how to reward sponsors
* Determine how to continue scaling
